from flask import Flask
import requests
from datetime import datetime

app = Flask(__name__)

# <p><center><b><i>CJ. Use Your Own Page Page And Not The Admin Page!</i></b></center></p><br><br>
global alert
alert = ""

def name_missing():
    return f"""
<!DOCTYPE HTML>
<html>
<head>
<title>Harmony Radio : RadioBoss Tools</title>
<style>@import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap')</style>
</head>
<body style="background-size: 1000% 1000%;background: #1e1e1e;font-family: 'Roboto', sans-serif;color: white;">
{alert}
<p><b>Missing Staff Name</b><br><br>Please enter the URL in the following format:<br>https://harmonyradio-radioboss-tools.vercel.app/[Name]</p>
</body>
</html>
"""

def on_air(name, j, p):
    a = ""
    minute = datetime.now().minute
    nwc = [30, 45, 50,55, 56, 57, 58, 59, 00]
    if minute in nwc:
        a = "<p><center><b>NWC Warning</b></center></p><br>"
    return f"""
<!DOCTYPE HTML>
<html>
<head>
<title>Harmony Radio : RadioBoss Tools</title>
<meta http-equiv="refresh" content="15;URL='https://harmonyradio-radioboss-tools.vercel.app/{name}'">
<style>@import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap')</style>
</head>
<body style="background-size: 1000% 1000%;background: #1e1e1e;font-family: 'Roboto', sans-serif;color: white;">
{alert}
<p><center><b>You Are On-Air</b></center></p><br>
{a}
<p>{j['now_playing']['song']['title']}<br>by {j['now_playing']['song']['artist']}</p>
<br>
<p>Listeners: {j['listeners']['total']}</p>
<br>
<p>Next Presenter: {p['two']['username']}</p>
</body>
</html>
"""
def soon(name, j, p):
    h = j["live"]["streamer_name"]
    if h == "":
        h = "AutoDJ"
    a = ""
    minute = datetime.now().minute
    mins_left = 60 - minute
    return f"""
<!DOCTYPE HTML>
<html>
<head>
<title>Harmony Radio : RadioBoss Tools</title>
<meta http-equiv="refresh" content="15;URL='https://harmonyradio-radioboss-tools.vercel.app/{name}'">
<style>@import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap')</style>
</head>
<body style="background-size: 1000% 1000%;background: #1e1e1e;font-family: 'Roboto', sans-serif;color: white;">
{alert}
<p><center><b>You Are On-Air Soon!</b></center></p><br>
<p>Your slot starts in {mins_left} min(s).</p>
<br>
<p>Listeners: {j['listeners']['total']}</p>
<br>
<p>Current Presenter: {h}</p>
</body>
</html>
"""
def regular(name):
    return f"""
<!DOCTYPE HTML>
<html>
<head>
<title>Harmony Radio : RadioBoss Tools</title>
<meta http-equiv="refresh" content="15;URL='https://harmonyradio-radioboss-tools.vercel.app/{name}'">
<style>@import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap')</style>
</head>
<body style="background-size: 1000% 1000%;background: #1e1e1e;font-family: 'Roboto', sans-serif;color: white;">
{alert}
<p><center><b>👋 Welcome {name}</b></center></p><br>
<p><center>You don't have slot coming up soon!</center></p>
</body>
</html>
"""

@app.route('/')
def home():
    return "V2 is no loger supported. Please use V3 at https://harmonytools.ml/"

@app.route('/gasystem/bB9mZc8vmzZmEb8vrDtezCSzRH7bwFasVrhLZXfEGv97nWbvvR/<message>')
def ga_system(message):
    return "V2 is no loger supported. Please use V3 at https://harmonytools.ml/"


@app.route('/<name>')
def page(name):
    return "V2 is no loger supported. Please use V3 at https://harmonytools.ml/"

